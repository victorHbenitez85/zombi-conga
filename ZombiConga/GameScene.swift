//
//  GameScene.swift
//  ZombiConga
//
//  Created by Victor Hugo Benitez Bosques on 29/08/16.
//  Copyright (c) 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    //2. create sprite node zombie
    let zombie = SKSpriteNode(imageNamed: "zombie1")
    
    /* 24 variables tener mayo control del tiempo*/
    var lastUpdatedTime : TimeInterval = 0 //Tener contol de cuando a sido la última vez que hemos actualizado la pantalla en el metodo úpdate -> tiempo exacto
    var dt : TimeInterval = 0 //Delta time : desde la última actualización  -> guarda la diferencia de tiempos
    
    // Declaracion de constantes del juego
    let zombiePixelPerSecond : CGFloat = 300 //movimiento del zombie 300px por segundo lineal
    var velocity = CGPoint.zero  //vector que se va a mover
    
    let playableArea : CGRect
    
    // Clase 29 se ejecuta antes de la creacion de la escena :: Cambiando el tamaño de la vista dependiendo de aspect ratio maximo
    override init(size: CGSize) {
        let maxAspectRatio : CGFloat = 16.0/9.0                   //maximo aspect radio a soportar 16:9 y el  minimo 3:2 -> calculamos el maximo
        let playableHieght = size.width / maxAspectRatio            //altura del juego disponible con respecto aspect ratio
        let playableMargin = (size.height - playableHieght)/2.0     //quitar margenes sobrantes al escalar al tamoño aspect ratio
        
        playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHieght)
        super.init(size: size) //debe llevar siempre super.init obligatorio
    }
    
    
    //al haver un problema con la funcion init requerida al sobre - escribirla
    required init?(coder aDecoder: NSCoder) {
        fatalError("El metodo init coder no ha sido implementado")
    }
    
    
    override func didMove(to view: SKView) {
        /*Este fragmento de codigo se ejecuta cuando se crea la vista
         En este fragmento se suele configurar: las variables de la escene, elementos iniciales de la escena
         */
        
        backgroundColor = SKColor.blue// Cambia el color de fondo al color especificado de la clase SKColor
        
        let background = SKSpriteNode(imageNamed: "background1")// 1. crear un sprite nodo de imagen
        
        // 1.1 cambiar el punto de anclaje de la imagen
        //background.anchorPoint = CGPointZero
        background.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        
        /*
         Rotacion de una imagen
         angulos en radianes = ¶ media vuelta, 2¶ vuelta completa
          CGFloat(M_PI)/8 rotamos la imagen 20 grados
         background.zRotation = CGFloat(M_PI)/8 */
        
        // 1.2 colocar la imagen en el punto inicial de la pantalla (0,0)
        background.position = CGPoint(x: size.width/2, y: size.height/2)
        //background.position = CGPointMake(size.width/2, size.height/2)
        
        // por defecto zPosition = 0  :: mandar al fondo el sprite -> -1
        background.zPosition = -1


        // 1.3 Agregarlo a la escena
        addChild(background)
        
        // 2.1 set propiedades del spriteNode
        zombie.position = CGPoint(x: 300, y: 400)
        
        // escalado dinamico usando tamaño de la pantalla :: proporcional 1/10 * size.width -> 10% del tamaño de la anchura de la pantalla

        zombie.xScale = 1
        zombie.yScale = 1
        
        // 2.2 Agregar a la escena
        addChild(zombie)
        
        let sizeBackground = background.size //obtener el tamaño del nodo creado (tamaño de la imagen)
        print("El tamo de la imagen es \(sizeBackground)")
    
    }

    
    override func update(_ currentTime: TimeInterval) {
        /* Se ejecuta juston antes de cada frame se vaya pintando en pantalla
            codigo ejecutado de manera periodica
            Clase 24. Creacion del movimiento del zombie
         */
        
        // velocidad de acuerdo al Delta time implementado : dt :: intervalo de tiempo que nos tenemos que mover
        if lastUpdatedTime > 0 {
            dt = currentTime - lastUpdatedTime //24.2 el lapso de tiempo de la ultima vez que se actualizo y el tiempo real que ocurrio el cambio
        }else{
            dt = 0
        }
        lastUpdatedTime = currentTime //24.3 actualizamos el tiempo de la ultima actualizacion
        print("La ultima actualizacion sera  \(dt * 1000) milisegundos")
        
       // zombie.position = CGPointMake(zombie.position.x + 5, zombie.position.y) //24.4 Cambiar la posicion del zombie : efecto que se mueve
        
        moveSprite(zombie, velocity: velocity) //movera el sprite a la velocidad de su propio ritmo
        
        checkBounds()

    }
    
    
    // Clase 25. movera el sprite con la velocidad dada
    func moveSprite(_ sprite:SKSpriteNode, velocity:CGPoint){
        
        // cantidad de movimiento que se va a desplazar el sprite : vector velocidad
        let amount = CGPoint(x: velocity.x * CGFloat(dt), y: velocity.y * CGFloat(dt))  //espacio = velocidad * tiempo: intervalo Delta time
        print("La cantidad que tenemos que movernos es \(amount)")
        
        sprite.position = CGPoint(x: sprite.position.x + amount.x, y: sprite.position.y + amount.y)// sumar la cantidad de movimiento a la posicion del sprite
    }
    
    /* Dada una localizacion donde el usuario toque  func moveZombieToLocation
      1.calcula el vector de movimiento de donde se encuentra el zombie
      2. Normaliza a norma 1
      3. re dimensiona segun la velocidad del zombie
      4. Que el usuario toque la pantalla
    */
    
    // Colocar un punto donde se debe mover el zombie
    func moveZombieToLocation(_ location : CGPoint ){
        
        // cantidad de movimiento hay que incrementar al zombie para que llegue donde hemos tocado fig. 25.2 Curso SpriteKit
        let offset = CGPoint(x: location.x - zombie.position.x, y: location.y - zombie.position.y) //apunta el zombie debe ir
        let offsetLength = sqrt(Double(offset.x * offset.x + offset.y * offset.y)) //longitud vector a donde debe ir
        let direction = CGPoint(x: offset.x/CGFloat(offsetLength), y: offset.y/CGFloat(offsetLength)) // Un vector unitario de movimiento
    
        velocity = CGPoint(x: direction.x * zombiePixelPerSecond, y: direction.y * zombiePixelPerSecond) // se indica la longitud del vector total que debe recorrer el sprite
        
    }
    
    func sceneTouched(_ touchLocation: CGPoint){
        moveZombieToLocation(touchLocation)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        /* Se ejecuta cuando el usuario toca la pantalla */
        // donde hemos tocado
        let touch = touches.first! as UITouch
        let location = touch.location(in: self) //dame el lugar donde se halla tocado en la pantalla ubicada en mi mismo
        sceneTouched(location)
        
    }

    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        // donde hemos tocado
        let touch = touches.first! as UITouch
        let location = touch.location(in: self) //dame el lugar donde se halla tocado en la pantalla ubicada en mi mismo
        sceneTouched(location)
    }
    
    //Clase 27 Las fronteras del juego colcar en update() fig. 27.1 Relacionado con CGREC : playableArea  fig 29
    func checkBounds(){
        
        //let bottomLeft = CGPointZero    //punto inicial de la frontera de juego
        let bottomLeft = CGPoint(x: 0, y: playableArea.minY) // posicion (0, cordenadaMinimaPLayableArea)
        let upperRight = CGPoint(x: size.width, y: playableArea.maxY) //coordenadas p(maximoAncho, maximaPlayableArea)
        
        if zombie.position.x <= bottomLeft.x{
            zombie.position.x = bottomLeft.x    //cambiamos al minimo permitido
            velocity.x = -velocity.x             //cambiamos coordenada a x
        }
        
        if zombie.position.x >= upperRight.x{
            zombie.position.x = upperRight.x //asignamos la posicion maxima
            velocity.x = -velocity.x    //rotamos la direccion
        }
        
        if zombie.position.y <= bottomLeft.y{
            zombie.position.y = bottomLeft.y    //cambiar al minimo que se permite
            velocity.y = -velocity.y            //cambiamos direccion a la veocidad
        }
        if zombie.position.y >= upperRight.y{
            zombie.position.y = upperRight.y    //maxima velocidad que se permite
            velocity.y = -velocity.y            //cambiamos direccion de la velocidad
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
}
